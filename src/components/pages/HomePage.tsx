import React,{ FC } from 'react';

const HomePage:FC = () => {
    return(
        <section className="section">
            <div className="container">
                <h1 className="title has-text-centered is-size-1 mb-6">Welcome</h1>
                <h2>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Eaque esse sapiente maiores iste quisquam corrupti voluptas 
                    mollitia quos veniam voluptatum facere nobis, minus nam, 
                    ipsum voluptatem nesciunt, reprehenderit dignissimos! Aut?</h2>
            </div>
        </section>
    )
}
export default HomePage;