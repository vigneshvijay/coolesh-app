# Getting Started with coolesh-app

Soon after cloning... head over to the terminal and hit npm install.


## App insights

This app uses firebase authentication.
Reproducing the following steps will make this app work like a charm

Step 1 : Head over to : https://console.firebase.google.com/u/0/

Step 2 : Create new project, name it and proceed

Step 3 : Once it is set up, click on the cog icon next to Project overview > Project settings > register the app

Step 4 : Copy the configuration details and save it in the .env file with relevant details.
```
REACT_APP_FIREBASE_API_KEY =  
REACT_APP_FIREBASE_AUTH_DOMAIN = 
REACT_APP_FIREBASE_PROJECT_ID = 
REACT_APP_FIREBASE_STORAGE_BUCKET = 
REACT_APP_MESSAGING_SENDER_ID = 
REACT_APP_FIREBASE_APP_ID = 
```

Step 5 : In the google firebase console,enable firestore database and update the rules as follows:

```
   match/users/{userId} {
   	allow read, update, delete : if request.auth !=null && request.auth.uid == userId;
    allow create : if true
   }
```

Step 6 : Kindly ensure Authentication is enabled in firebase project.

Step 7 : Test out the app on your machine.



### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### About

This app has been composed using TS, react-redux. 


